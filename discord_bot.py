import discord
from gtts import gTTS
import re
from discord.ext import commands
import os
import pickledb
import json
from repattern import JP, TH

client = discord.Client()

TEST_WORDS = [u'テスト', u'診断', u'試験', u'検査', u'ทดสอบ', 'test']
DEBUG = False

def isascii(s):
    """Check if the characters in string s are in ASCII, U+0-U+7F."""
    return len(s) == len(s.encode())

@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))

@client.event
async def on_message(message):
    dbname = F'server-{message.guild.id}.db'
    try:
        db = pickledb.load(dbname, False)
    except json.decoder.JSONDecodeError:
        os.remove(dbname)
        db = pickledb.load(dbname, False)
    if db.get(F'{message.channel.id}'):
        user_exists = message.author.id in db.get(F'{message.channel.id}')
    else:
        user_exists = False
    if message.content.startswith('\\echo online'):
        await message.channel.send('Echo Online!')
    
    elif message.content.startswith('\\echo toggle'):
        try:
            if ch_users:=db.get(F'{message.channel.id}'):
                ch_users = set(ch_users)
                if message.author.id in ch_users:
                    ch_users -= {message.author.id}
                    delete = True
                else:
                    ch_users = ch_users.union({message.author.id})
                    delete = False
                db.set(F'{message.channel.id}', list(ch_users)) 
            else:
                db.set(F'{message.channel.id}', [message.author.id])
                delete = False
            db.dump()
            if not delete:
                #await ctx.reply(F'Enabled auto TTS on this text channel for {message.author.display_name}.', mention_author=True)
                await message.channel.send('Start auto TTS on this text channel for user: ' + message.author.display_name)
            else:
                #await ctx.reply(F'Disabled auto TTS on this text channel for {message.author.display_name}.', mention_author=True)
                await message.channel.send('Disabled auto TTS on this text channel for user: ' + message.author.display_name)
        except:
            await message.channel.send('Echo database error!')

    elif message.content.startswith('\\echo talk') or user_exists and (not message.content.startswith('\\')):
        if message.content.startswith('\\echo talk'):
            msg = message.content.split('\\echo talk ')[1]
        else:
            msg = message.content
        msg = msg.strip() #delete whitespace
        if msg.lower() in TEST_WORDS:
            msg = u'วันพระวันเจ้าไม่เว้นกันเลย สรรหาแต่จะทดสอบ'
        elif re.findall(r'(https?://[^\s]+)', msg) or message.attachments: #detect url link
            return 
        msg = re.sub(r'<:\w*:\d*>', ' ', msg) #delete emoji
        lang = 'th'
        if (msg.startswith(".") and re.match(TH, msg)) or len(msg) == msg.count('.'):
            msg = msg.replace(".", u"จุด")
        if not isascii(msg):
            if re.match(TH, msg):
                lang = 'th'
                 #replace dot with more triggerred word
            elif re.match(JP, msg):
                lang = 'ja'
        else:
            lang = 'en'
        if len(msg) >= 180:
            msg = u'ยาวไปไม่อ่าน'
        if DEBUG:
            print(message)
        user = message.author
        voice_channel = user.voice.channel
        filename = 'tts-' + str(voice_channel.id) + '.mp3'
        user = message.author
        #song_there = os.path.isfile(filename)
        try:
            await voice_channel.connect()
        except Exception as e:
            if DEBUG:
                print(e)
        voice = discord.utils.get(client.voice_clients, guild=message.guild)
        tts = gTTS(text=msg, lang=lang)
        tts.save(filename)
        if voice.is_playing():
            try:
                voice.stop()
            except:
                pass
        try:
            voice.play(discord.FFmpegPCMAudio(filename, executable=os.path.join('..', 'ffmpeg', 'bin', 'ffmpeg.exe')))
        except:
            voice.play(discord.FFmpegPCMAudio(filename, executable='ffmpeg'))
        
    elif message.content.startswith('\\echo bye'):
        try:
            guild = message.guild.voice_client
            await guild.disconnect()
        except Exception as e:
            print(e)

    elif message.content.startswith('\\echo'):
        await message.channel.send(
            u'- \\echo = Display this message\n' +
            u'- \\echo online = For check status. If no response, oɥɔƎ eliminated.\n' +
            u'- \\echo talk <text to talk> = Force talk the text\n' +
            u'- \\echo toggle = Auto read user massage\n' +
            u'- \\echo bye = Disconnect oɥɔƎ\n'
            )

try:
    from local import token
    client.run(token)
except:
    print('Not found token in local.py')